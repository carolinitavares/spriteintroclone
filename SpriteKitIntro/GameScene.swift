//
//  GameScene.swift
//  SpriteKitIntro
//
//  Created by MacStudent on 2019-02-06.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {

    // Example 1  - Adding text to the screen
    let label = SKLabelNode(text:"HELLO WORLD!")
    let label2 = SKLabelNode(text:"CAROL MODIFICATION")
    
    // Example 2 - Draw a square on the screen
    let square = SKSpriteNode(color: SKColor.blue, size: CGSize(width: 50, height: 50))
    
    // Example 3 - Draw an image on the screen
    let duck = SKSpriteNode(imageNamed: "psyduck")
    
    // Example 4 - Draw a circle on the screen
    let circle = SKShapeNode(circleOfRadius: 40)
    let circle1 = SKShapeNode(circleOfRadius: 20)
    
    override func update(_ currentTime: TimeInterval) {
        //print("time: \(currentTime)")
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            let node = atPoint(location)
            
            let middleOfScreen = self.size.width/2
            if (location.x < middleOfScreen){
                print("LEFT")
            }else{
                print("RIGHT")
            }
        }
    }
    
    override func didMove(to view: SKView) {
        // output the size of the screen
        print("Screen size (w,h): \(size.width),\(size.height)")
        
        // Add images to the scene
        let bug = SKSpriteNode(imageNamed: "caterpie")
        bug.position = CGPoint(x:size.width/2, y:size.height/2)
        addChild(bug)
        
        duck.position = CGPoint(x:size.width/2+100, y:size.height/2)
        addChild(duck)
        
        
        // configure your text
        label.position = CGPoint(x:size.width/2, y:size.height/2)
        label.fontSize = 45
        label.fontColor = SKColor.yellow

        label2.fontSize = 60
        label2.position = CGPoint(x:size.width/2, y:200)
        
        // add it to your scene (draw it!)
        addChild(label)
        addChild(label2)
        
        // configure the square
        square.position = CGPoint(x: 105, y:700);
        // add square to scene
        addChild(square)
        
        // configure your circle
        // -----------------------
        // color of border
        circle.strokeColor = SKColor.yellow
        // width of border
        circle.lineWidth = 5
        // fill color
        circle.fillColor = SKColor.magenta
        // location of circle
        circle.position = CGPoint(x:200, y:100)
        addChild(circle)
        
        // configure your circle
        // -----------------------
        // color of border
        circle1.strokeColor = SKColor.blue
        // width of border
        circle1.lineWidth = 10
        // fill color
        circle1.fillColor = SKColor.lightGray
        // location of circle
        circle1.position = CGPoint(x:0, y:self.size.height/2)
        addChild(circle1)
        
        print("Width \(self.size.width)")
        print("Height \(self.size.height)")
        
        // move pixels distance
        let moveAction = SKAction.moveBy(x: 300, y: 0, duration: 3)
        //circle1.run(moveAction)
        
        // move to a specific x location
        let moveAction2 = SKAction.moveTo(x: 0, duration: 2)
        circle.run(moveAction2)
        
        // move to a location with CGPoint
        let newPosition = CGPoint(x: 255, y: 300)
        let moveAction3 = SKAction.move(to: newPosition, duration: 3)
        square.run(moveAction3)
        
        //doing a sequence of movements and reversed sequence (actions works too)
        let sequence: SKAction = SKAction.sequence([moveAction, moveAction.reversed(), moveAction3, moveAction2])
        circle1.run(sequence)
        //duck.run(sequence.reversed())
        
        let sequence2: SKAction = SKAction.sequence([sequence, sequence.reversed()])
        //circle1.run(SKAction.repeatForever(sequence2)) //repeats forever
        //circle1.run(SKAction.repeat(sequence, count: 2)) // repeats 2x
        
        
        //NOT WORKING
        if (circle1.frame.intersects(square.frame)){
            print("COLLISION DETECTED!")
        }
        if circle1.frame.intersects(square.frame){
            print("COLLISION")
        }
        
    }
    
}
